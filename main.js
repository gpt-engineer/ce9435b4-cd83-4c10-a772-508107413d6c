document.addEventListener('DOMContentLoaded', () => {
  const prepopulatedTasks = [
    'Check patient history',
    'Prepare dental instruments',
    'Clean treatment room',
    'Sterilize instruments',
    'Assist in dental procedures',
  ];

  const taskForm = document.getElementById('task-form');
  const taskInput = document.getElementById('task-input');
  const prepopulatedTasksSection = document.getElementById('prepopulated-tasks');
  const newTasksSection = document.getElementById('new-tasks');

  // Function to create a new task
  function createTask(task) {
    const taskDiv = document.createElement('div');
    taskDiv.classList.add('flex', 'justify-between', 'items-center', 'bg-white', 'p-4', 'mb-2', 'rounded', 'shadow');

    const taskText = document.createElement('input');
    taskText.type = 'text';
    taskText.value = task;
    taskText.classList.add('text-lg', 'bg-transparent', 'outline-none', 'whitespace-normal');
    taskText.readOnly = true;
    taskText.addEventListener('click', () => {
      taskText.readOnly = !taskText.readOnly;
    });

    const taskButtons = document.createElement('div');

    const doneButton = document.createElement('button');
    doneButton.innerHTML = '<i class="fas fa-check text-green-500"></i>';
    doneButton.classList.add('mr-2');
    doneButton.addEventListener('click', () => {
      taskText.classList.toggle('line-through');
    });

    const deleteButton = document.createElement('button');
    deleteButton.innerHTML = '<i class="fas fa-trash text-red-500"></i>';
    deleteButton.addEventListener('click', () => {
      taskDiv.remove();
    });

    taskButtons.appendChild(doneButton);
    taskButtons.appendChild(deleteButton);

    taskDiv.appendChild(taskText);
    taskDiv.appendChild(taskButtons);

    return taskDiv;
  }

  // Add prepopulated tasks
  prepopulatedTasks.forEach((task) => {
    const taskDiv = createTask(task);
    prepopulatedTasksSection.appendChild(taskDiv);
    // Button to add prepopulated task
    const addButton = document.createElement('button');
    addButton.textContent = 'Add Task';
    addButton.classList.add('bg-blue-500', 'text-white', 'rounded', 'p-2');
    addButton.addEventListener('click', () => addPrepopulatedTask(task));
    taskDiv.appendChild(addButton);
  });

  // Function to add prepopulated task
  function addPrepopulatedTask(task) {
    const taskDiv = createTask(task);
    newTasksSection.appendChild(taskDiv);
  }

  // Add new task
  taskForm.addEventListener('submit', (e) => {
    e.preventDefault();

    if (taskInput.value.trim() === '') {
      alert('Please enter a task');
      return;
    }

    const taskDiv = createTask(taskInput.value);
    newTasksSection.appendChild(taskDiv);

    taskInput.value = '';
  });
});
